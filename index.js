console.log("Hello Kris");

// For selecting HTML elements, we will be using the document.querySelector()
// Syntax: document.querySelector("htmlElement")
// document - refers to the whole page
// .querySelector - used to select a specific object(HTML element) from the document/webpage
	// The querySelector() takes a string input that is formatted like a CSS selector when applying the styles
const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

// Event Listener
	// Whenever a user interacts with a webpage, this action is considered as an "event"
	// To performe an action when a event triggers, we need to lisen to it.
	// The method addEventListener takes two argumanets:
		// - A sting indentifying an event
		// - A function that the listener will execute one the "event" is triggered
// "event" contains the information on the triggered event
txtFirstName.addEventListener("keyup", (event) => {

	// .value - sets or returns the value of the element
	// .innerHTML - sets or return the value of an attribute of an element
	spanFullName.innerHTML = txtFirstName.value;

})

txtFirstName.addEventListener("keyup", (event) => {

	// The event.target containe the element where the event happened
	console.log(event.target);

	// The event.target.value gets the value of the input object
	console.log(event.target.value);
})

txtLastName.addEventListener("keyup", (event) => {
	spanFullName.innerHTML = txtLastName.value;

})